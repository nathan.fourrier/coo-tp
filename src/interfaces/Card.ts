export interface Card{
    id:String,
    url:String,
    description:String,
    image:String
}
