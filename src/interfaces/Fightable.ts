import Coordinates from '../class/Coordinates';
import Metric from '../class/Metric';

export default interface Fightable {
    id:Number,
    health:Number,
    manaLevel:Number,
    forceLevel:Number,
    coordinates:Coordinates,
    items: String[],
    weight:Metric,
    height:Metric
// eslint-disable-next-line semi
}
