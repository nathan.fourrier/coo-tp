import storage from 'node-persist';
import UserV1 from './class/UserV1';
import Character from './class/Character';
import Item from './class/Item';
import ItemAttribute from './class/ItemAttribute';
import Coordinates from './class/Coordinates';
import { db } from './db';
import { initStorage } from './storage';
import Metric from './class/Metric';
import UserV2 from './class/UserV2';

(async () => {
  await initStorage();

  await storage.setItem('users', [
    new UserV1(1, 'jeankevindu59', '69', ['69', '70']),
  ]);

  await storage.setItem('characters', [
    new Character(
      69,
      'Tapefort',
      1500,
      100,
      100,
      100,
      new Coordinates(-1, 42, 984),
      ['item_1'],
      new Metric('kg', 75.8),
      new Metric('cm', 177),
    ),
    new Character(
      70,
      'Pasouf',
      0,
      10,
      2,
      1,
      new Coordinates(0, 0, 0),
      ['item_2'],
      new Metric('kg', 75.8),
      new Metric('cm', 177),
    ),
  ]);

  await storage.setItem('items', [
    new Item(
      'item_1',
      'Epée magique',
      25,
      'SWORD',
      [
        new ItemAttribute('mana', '+50'),
        new ItemAttribute('intelligence', '-25'),
      ],
    ),
    new Item(
      'item_2',
      'Epée de merde en bois',
      25,
      'SWORD',
      [],
    ),
    new Item(
      'item_3',
      'Bâton qui fait des trucs',
      6,
      'STICK',
      [new ItemAttribute('mana', '+10'),

      ],
    ),
  ]);

  await storage.setItem('mobs', [
    {
      id: 1,
      type: 'ARCHER',
      health: 100,
      manaLevel: 100,
      forceLevel: 100,
      coordinates: {
        x: -1,
        y: 42,
        z: 984,
      },
      items: ['item_1'],
      weight: {
        unit: 'kg',
        value: 75.8,
      },
      height: {
        unit: 'cm',
        value: 177,
      },
    },
    {
      id: 2,
      type: 'WIZARD',
      health: 100,
      manaLevel: 100,
      forceLevel: 100,
      coordinates: {
        x: -1,
        y: 42,
        z: 984,
      },
      items: ['item_1'],
      weight: {
        unit: 'kg',
        value: 75.8,
      },
      height: {
        unit: 'cm',
        value: 177,
      },
    },
    {
      id: 3,
      type: 'ORC',
      health: 100,
      manaLevel: 100,
      forceLevel: 100,
      coordinates: {
        x: -1,
        y: 42,
        z: 984,
      },
      items: ['item_1'],
      weight: {
        unit: 'kg',
        value: 75.8,
      },
      height: {
        unit: 'cm',
        value: 177,
      },
    },
  ]);

  db.default({
    users: [
      new UserV2(1, 'jeankevindu59', 69, [69, 70]),
    ],
  });
})();
