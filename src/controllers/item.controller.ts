import { Request, Response } from 'express';
import { ItemModel } from '../models';

// All items in database
const allItems = async (_:Request, res:Response) => {
  const items = await ItemModel.fetchItems();
  res.json({ items });
};

// Single item by id
const singleItem = async (req:Request, res:Response) => {
  const { id } = req.params;
  const item = await ItemModel.fetchItem(id);
  res.json({ item });
};

// Remove item by id
const removeItem = async (req:Request, res:Response) => {
  const { id } = req.params;
  const item = await ItemModel.fetchItem(id);
  if (item === undefined) {
    res.status(404).json('Item not found');
  } else {
    await ItemModel.removeItem(id);
    res.status(200).json(item);
  }
};

// Create a new item
const createItem = async (req:Request, res:Response) => {
  const { item } = req.body;
  ItemModel.addItem(item)
    .then(() => res.status(201).json({ item }))
    .catch((err) => res.status(400).json({ err }));
};

export default {
  allItems, singleItem, removeItem, createItem,
};
