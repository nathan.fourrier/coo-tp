import { Request, Response } from 'express';
import storage from 'node-persist';
import Character from '../class/Character';

import { CharacterModel } from '../models';
import characterModels from '../models/characterModels';

// Get all characters of the database
const allCharacters = async (_:Request, res:Response) => {
  const characters = await CharacterModel.fetchCharacters();
  res.json({ characters });
};

// Get a single character by id
const singleCharacter = async (req:Request, res:Response) => {
  const { id } = req.params;

  try {
    const character = CharacterModel.fetchCharacter(id);

    if (!character) {
      throw new Error('Not found');
    }

    res.json({ character });
  } catch (err) {
    res.sendStatus(404);
  }
};

// Get the list of the items of a character
const itemsList = async (req:Request, res:Response) => {
  const { id } = req.params;

  try {
    const items = await CharacterModel.fetchItems(id);
    res.json({ items });
  } catch (err) {
    res.sendStatus(404);
  }
};

// Add item to a character
const addItem = async (req:Request, res:Response) => {
  const { id } = req.params;
  const { itemId } = req.body;

  try {
    if (!id) {
      res
        .status(418)
        .json('Please specify correct id in url');
    } else {
      CharacterModel.addItem(id, itemId)
        .then((items:String[]) => res.status(201).json({ items }));
    }
  } catch {
    res.sendStatus(404);
  }
};

// Remove an item of a character
const removeItem = async (req:Request, res:Response) => {
  const { id, itemId } = req.params;
  try {
    if (!id) {
      res
        .status(418)
        .json('Please specify correct id in url');
    } else {
      CharacterModel.removeItem(id, itemId)
        .then((items:String[]) => res.status(202).json({ items }));
    }
  } catch {
    res.sendStatus(404);
  }
};

// Update the properties of a character
const updateCharacter = async (req:Request, res:Response) => {
  const { id } = req.params;
  const propertiesToUpdate = { ...req.body };

  const characters:Character[] = await storage
    .getItem('characters')
    .catch(() => res.sendStatus(404));

  if (!id) {
    res
      .status(418)
      .json('Please specify correct id in url');
  } else if (characters.filter((elt: Character) => `${elt.id}` === id).length === 0) {
    // Prevent update of a non existing character
    res
      .status(404)
      .json('Character not found');
  } else if (propertiesToUpdate.id || propertiesToUpdate.items) {
    // It's forbidden to update id or items list
    res
      .status(403)
      .json(`You can't update ${propertiesToUpdate.id ? 'id' : 'items'}`);
  } else {
    const character = await characterModels.fetchCharacter(id);

    const newCharacter = new Character(
      character.id,
      propertiesToUpdate.name ? propertiesToUpdate.name : character.name,
      propertiesToUpdate.goldAmount ? propertiesToUpdate.goldAmount : character.goldAmount,
      propertiesToUpdate.health ? propertiesToUpdate.health : character.health,
      propertiesToUpdate.manaLevel ? propertiesToUpdate.manaLevel : character.manaLevel,
      propertiesToUpdate.forceLevel ? propertiesToUpdate.forceLevel : character.forceLevel,
      propertiesToUpdate.coordinates ? propertiesToUpdate.coordinates : character.coordinates,
      propertiesToUpdate.items ? propertiesToUpdate.items : character.items,
      propertiesToUpdate.weight ? propertiesToUpdate.weight : character.weight,
      propertiesToUpdate.height ? propertiesToUpdate.height : character.height,
    );

    CharacterModel.updateCharacter(id, newCharacter)
      .then((updatedCharacter:Character) => res.status(202).json({ character: updatedCharacter }));
  }
};
export default {
  allCharacters, singleCharacter, itemsList, addItem, removeItem, updateCharacter,
};
