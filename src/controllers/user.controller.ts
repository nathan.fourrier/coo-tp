import { Request, Response } from 'express';
import { UserModel, CharacterModel } from '../models';
import Character from '../class/Character';

// All users in database
const allUsers = async (_:Request, res:Response) => {
  const { version } = res.locals;
  const users = await UserModel.fetchUsers(version);

  res.json({ users });
};

// Single user by id
const singleUser = async (req:Request, res:Response) => {
  const { id } = req.params;

  try {
    const { version } = res.locals;

    const user = UserModel.fetchUser(id, version);
    res.json({ user });
  } catch (err) {
    res.sendStatus(404);
  }
};

// Characters of a user
const charactersList = async (req:Request, res:Response) => {
  const { id } = req.params;

  try {
    const { version } = res.locals;

    const characters = await CharacterModel.fetchCharacters();

    const user = await UserModel.fetchUser(id, version);

    if (!user) {
      throw new Error('Not found');
    }

    const userCharacters = characters
      .filter((character: Character) => user.characters.includes(character.id));

    res.json({ characters: userCharacters });
  } catch (err) {
    res.sendStatus(404);
  }
};

export default { allUsers, singleUser, charactersList };
