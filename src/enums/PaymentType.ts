/* eslint-disable no-shadow */
/* eslint-disable no-unused-vars */
export enum PaymentType{
    PAYPAL = 'PAYPAL',
    CREDIT_CARD = 'CREDIT_CARD',
    GIFT_CARD = 'GIFT_CARD'
}
