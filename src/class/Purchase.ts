/* eslint-disable class-methods-use-this */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
/* eslint-disable no-useless-constructor */
export class Purchase {
  constructor(
        private id : String,
        private amount : Number,
        private date : Date,
  ) {}
}
