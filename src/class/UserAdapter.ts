/* eslint-disable camelcase */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
/* eslint-disable no-useless-constructor */
import User from '../interfaces/User';
import { PaymentMean } from './PaymentMean';
import { Purchase } from './Purchase';
import UserV1 from './UserV1';

export default class UserAdapter implements User {
  private wallet: PaymentMean[]

  private purchases: Purchase[]

  id:String

  old_id:Number

  username:String

  playedCharacter:Number

  characters: Number[]

  constructor(user :UserV1) {
    this.id = '00000000-0000-0000-0000-000000000000';
    this.old_id = user.id;
    this.username = user.name;
    this.playedCharacter = Number(user.selectedCharacter);
    this.characters = user.characters.map(Number);

    this.wallet = [];
    this.purchases = [];
  }

  getWallet() {
    return this.wallet;
  }

  getPurchases() {
    return this.purchases;
  }
}
