import ItemAttribute from './ItemAttribute';

export default class Item {
    id:String

    name:String

    price:Number

    type:String

    attributes:ItemAttribute[]

    constructor(id:String, name:String, price:Number, type:String, attributes:ItemAttribute[]) {
      this.id = id;
      this.name = name;
      this.price = price;
      this.type = type;
      this.attributes = attributes;
    }
}
