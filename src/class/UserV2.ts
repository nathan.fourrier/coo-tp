/* eslint-disable camelcase */
/* eslint-disable class-methods-use-this */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
/* eslint-disable no-useless-constructor */
import { v4 as uuid } from 'uuid';
import User from '../interfaces/User';
import { PaymentMean } from './PaymentMean';
import { Purchase } from './Purchase';

export default class UserV2 implements User {
  private wallet: PaymentMean[]

  private purchases: Purchase[]

  id:String

  old_id:Number

  username:String

  playedCharacter:Number

  characters: Number[]

  constructor(old_id:Number, username:String, playedCharacter:number, characters:Number[]) {
    this.id = uuid();
    this.old_id = old_id;
    this.username = username;
    this.playedCharacter = playedCharacter;
    this.characters = characters;

    this.wallet = [];
    this.purchases = [];
  }

  getWallet() {
    return this.wallet;
  }

  getPurchases() {
    return this.purchases;
  }
}
