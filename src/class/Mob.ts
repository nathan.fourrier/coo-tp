import Coordinates from './Coordinates';
import Metric from './Metric';
import Fightable from '../interfaces/Fightable';

export default class Mob implements Fightable {
    id:Number

    type:String

    health:Number

    manaLevel:Number

    forceLevel:Number

    coordinates:Coordinates

    items:String[]

    weight: Metric

    height: Metric

    constructor(id:Number, type:String, health:Number,
      manaLevel:Number, forceLevel:Number, coordinates:Coordinates,
      items:String[], weight: Metric, height: Metric) {
      this.id = id;
      this.type = type;
      this.health = health;
      this.manaLevel = manaLevel;
      this.forceLevel = forceLevel;
      this.coordinates = coordinates;
      this.items = items;
      this.weight = weight;
      this.height = height;
    }
}
