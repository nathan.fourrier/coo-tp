/* eslint-disable class-methods-use-this */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
/* eslint-disable no-useless-constructor */
import { PaymentMean } from './PaymentMean';
import { Purchase } from './Purchase';

export default class UserV1 {
  private wallet: PaymentMean[]

  private purchases: Purchase[]

  id:Number

  name:String

  selectedCharacter:String

  characters: String[]

  constructor(id:Number, name:String, selectedCharacter:String, characters:String[]) {
    this.id = id;
    this.name = name;
    this.selectedCharacter = selectedCharacter;
    this.characters = characters;
    this.wallet = [];
    this.purchases = [];
  }

  getWallet() {
    return this.wallet;
  }

  getPurchases() {
    return this.purchases;
  }
}
