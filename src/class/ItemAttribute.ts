export default class ItemAttribute {
    type:String

    value:String

    constructor(type:String, value:String) {
      this.type = type;
      this.value = value;
    }
}
