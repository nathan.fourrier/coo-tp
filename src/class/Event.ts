/* eslint-disable class-methods-use-this */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
/* eslint-disable no-useless-constructor */
import { Card } from '../interfaces/Card';

export default class Event implements Card {
  constructor(
        public id:String,
        public url:String,
        public description:String,
        public image:String,
  ) {}

  public register(userId: String) {
  }
}
