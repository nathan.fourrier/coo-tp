export default class Coordinates {
    x:Number

    y:Number

    z:Number

    constructor(x:Number, y:Number, z:Number) {
      this.x = x;
      this.y = y;
      this.z = z;
    }
}
