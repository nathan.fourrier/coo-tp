/* eslint-disable class-methods-use-this */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
/* eslint-disable no-useless-constructor */
import { PaymentType } from '../enums/PaymentType';

export class PaymentMean {
  constructor(
        private id: String,
        private type: PaymentType,
        private informations: Record<String, unknown>,
  ) {}
}
