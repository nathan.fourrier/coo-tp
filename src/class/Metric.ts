export default class Metric {
    unit:String

    value: Number

    constructor(unit:String, value:Number) {
      this.unit = unit;
      this.value = value;
    }
}
