/* eslint-disable class-methods-use-this */
/* eslint-disable no-empty-function */
/* eslint-disable no-unused-vars */
/* eslint-disable no-useless-constructor */
export default class News {
  constructor(
        public id:String,
        public url:String,
        public description:String,
        public image:String,
  ) {}
}
