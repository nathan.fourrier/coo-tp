import Coordinates from './Coordinates';
import Metric from './Metric';
import Fightable from '../interfaces/Fightable';

export default class Character implements Fightable {
    id:Number

    name:String

    goldAmount:Number

    health:Number

    manaLevel:Number

    forceLevel:Number

    coordinates:Coordinates

    items: String[]

    weight:Metric

    height:Metric

    constructor(id:Number, name:String, goldAmount:Number,
      health:Number, manaLevel:Number, forceLevel:Number,
      coordinates:Coordinates, items:String[], weight:Metric,
      height:Metric) {
      this.id = id;
      this.name = name;
      this.goldAmount = goldAmount;
      this.health = health;
      this.manaLevel = manaLevel;
      this.forceLevel = forceLevel;
      this.coordinates = coordinates;
      this.items = items;
      this.weight = weight;
      this.height = height;
    }
}
