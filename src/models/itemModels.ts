import storage from 'node-persist';
import { CharacterModel } from '.';
import Character from '../class/Character';
import Item from '../class/Item';

const fetchItems = ():Promise<Item[]> => storage.getItem('items');

const fetchItem = async (id:String) => {
  const items = await fetchItems();
  const [item] = items.filter((elt:Item) => elt.id === id);
  return item;
};

const addItem = async (item:Item) => {
  const items = await storage.getItem('items');
  items.push(item);
  return storage.setItem('items', items);
};

const removeItem = async (id:String) => {
  // Removing the item to every characters
  const characters = await CharacterModel.fetchCharacters();

  // Need to execute promises in sequence because of node-persist
  await characters.reduce(async (previousPromise:Promise<String[]>,
    nextCharacter:Character) => {
    await previousPromise;
    return CharacterModel.removeItem(`${nextCharacter.id}`, id);
  },
  Promise.resolve([]));

  // Removing the item entity
  let items:Item[] = await storage.getItem('items');
  items = items.filter((item:Item) => item.id !== id);
  await storage.setItem('items', items);
};

export default {
  fetchItems, fetchItem, addItem, removeItem,
};
