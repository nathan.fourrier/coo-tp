import storage from 'node-persist';
import User from '../interfaces/User';
import UserV1 from '../class/UserV1';
import { db } from '../db';
import UserAdapter from '../class/UserAdapter';

const fetchUsersV1 = ():Promise<UserV1[]> => storage.getItem('users');

const fetchUsersV2 = ():Promise<User[]> => db.get('users').value();

const fetchUsers = async (version:String) => {
  if (version === 'v1') {
    const usersV1 = await fetchUsersV1();
    return usersV1.map((usr:UserV1) => <User> new UserAdapter(usr));
  }
  return fetchUsersV2();
};

const fetchUser = async (id:String, version:String) => {
  const users = await fetchUsers(version);
  return users.find((user: User) => String(user.id) === id);
};

export default { fetchUsers, fetchUser };
