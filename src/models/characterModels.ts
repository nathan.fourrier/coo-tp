import storage from 'node-persist';
import Character from '../class/Character';
import { CharacterModel, ItemModel } from '.';

const fetchCharacters = ():Promise<Character[]> => storage.getItem('characters');

const fetchCharacter = async (id:String) => {
  const characters = await fetchCharacters();
  const [character] = characters.filter((elt:Character) => `${elt.id}` === id);
  if (!character) {
    throw new Error('Not found');
  }
  return character;
};

const updateCharacter = async (id:String, newCharacter:Character) => {
  let characters = await fetchCharacters();

  characters = characters.map((character: Character) => {
    if (`${character.id}` === id) {
      const updatedCharacter:Character = newCharacter;
      return updatedCharacter;
    }
    return character;
  });
  await storage.setItem('characters', characters);
  return newCharacter;
};

const fetchItems = async (id:String) => {
  const character = await CharacterModel.fetchCharacter(id);

  const items = await Promise.all(
    character.items
      .map((elt:String) => ItemModel.fetchItem(elt)),
  );

  return items;
};

const addItem = async (characterId:String, itemId:String) => {
  const item = await ItemModel.fetchItem(itemId);
  if (!item) {
    throw new Error('Item not found');
  }

  const characterToUpdate = fetchCharacter(characterId);
  if (!characterToUpdate) {
    throw new Error('Character not found');
  }

  let characters = await fetchCharacters();
  let updatedItems:String[] = [];
  characters = characters.map((character: Character) => {
    if (`${character.id}` === characterId) {
      let { items } = character;
      items.push(itemId);
      items = [...new Set(items)];
      const updatedCharacter = { ...character, items };
      updatedItems = items;
      return updatedCharacter;
    }
    return character;
  });

  await storage.setItem('characters', characters);

  return updatedItems;
};

const removeItem = async (characterId:String, itemId:String) => {
  let characters = await fetchCharacters();
  let updatedItems:String[] = [];

  characters = characters.map((character: Character) => {
    if (`${character.id}` === characterId) {
      let { items } = character;
      items = items.filter((item:String) => item !== itemId);
      const updatedCharacter = { ...character, items };
      updatedItems = items;
      return updatedCharacter;
    }

    return character;
  });
  await storage.setItem('characters', characters);
  return updatedItems;
};

export default {
  fetchCharacters, fetchCharacter, updateCharacter, fetchItems, addItem, removeItem,
};
