import storage from 'node-persist';

export async function initStorage() {
  await storage.init({
    dir: './data/node-persist',
    stringify: JSON.stringify,
    parse: JSON.parse,
    encoding: 'utf8',
    logging: true,
  });
}
