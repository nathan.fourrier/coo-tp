import { Router } from 'express';
import { ItemController } from '../controllers';

const router = Router();

router.get('/', ItemController.allItems);

router.get('/:id', ItemController.singleItem);

router.delete('/:id', ItemController.removeItem);

router.post('/', ItemController.createItem);

export default router;
