import { Router } from 'express';
import CharactersRouter from './characters.routes';
import UserRouter from './users.routes';
import ItemRouter from './items.routes';
import { versionChecker } from '../middlewares';

export function createRouter() {
  const router = Router();
  router.use('/:version', versionChecker);
  router.use('/:version/characters', CharactersRouter);
  router.use('/:version/users', UserRouter);
  router.use('/:version/items', ItemRouter);

  return router;
}
