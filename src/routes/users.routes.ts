import { Router } from 'express';
import { UserController } from '../controllers';

const router = Router();

router.get('/', UserController.allUsers);

router.get('/:id', UserController.singleUser);

router.get('/:id/characters', UserController.charactersList);

export default router;
