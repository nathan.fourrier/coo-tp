import { Router } from 'express';
import { CharacterController } from '../controllers';

const router = Router();

// Get all characters
router.get('/', CharacterController.allCharacters);

// Get single character by id
router.get('/:id', CharacterController.singleCharacter);

// Get items of a character
router.get('/:id/items', CharacterController.itemsList);

// Add item to a character
router.post('/:id/items', CharacterController.addItem);

// Remove item of a character
router.delete('/:id/items/:itemId', CharacterController.removeItem);

// Update a character
router.put('/:id', CharacterController.updateCharacter);

export default router;
