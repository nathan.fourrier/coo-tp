/* eslint-disable new-cap */
import stormdb from 'stormdb';

const engine = new stormdb.localFileEngine('./data/db/db.stormdb');

export const db = new stormdb(engine);
