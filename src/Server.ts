import { Application } from 'express';
import { createServer, Server } from 'http';

export class HttpServer {
  readonly #port: Number;

  readonly #server: Server;

  constructor(port: Number, app: Application) {
    this.#port = port;
    this.#server = createServer(app);
  }

  start() {
    this.#server.listen(this.#port);
  }

  public get port() {
    return this.#port;
  }
}
