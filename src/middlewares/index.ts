import { Request, Response } from 'express';

export const versionChecker = (req:Request, res:Response, next:any) => {
  const { version } = req.params;
  res.locals.version = version;
  next();
};
