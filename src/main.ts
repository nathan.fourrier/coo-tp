import { HttpServer } from './Server';
import { App } from './App';
import { createRouter } from './routes';
import { initStorage } from './storage';
// Inject data to local storage
import './data';

const server = new HttpServer(3000, new App(createRouter()).instance);
initStorage()
  .then(() => server.start())
  .catch(() => console.error('Unable to init storage !!'));
